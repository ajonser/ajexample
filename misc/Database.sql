CREATE DATABASE `ajexample`;
GRANT ALL PRIVILEGES ON ajexample.* TO 'ajexample'@127.0.0.1 IDENTIFIED BY 'unsecurePassw0rD!';

USE `ajexample`;
CREATE TABLE `rates` (
  `timestamp` int(11) NOT NULL,
  `currency` ENUM('USD','CHF') NOT NULL,
  `rate` DECIMAL(10,6) UNSIGNED,
  PRIMARY KEY (`timestamp`, `currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*
Als Primary-key wurde der Timestamp und die Währung gewählt, 
damit pro Sekunde und Währung nur ein Wert existieren kann.

Decimal wurde gewählt, weil man hier nur mit exakten Werten
arbeiten darf, float ist nur angenähert. (Bsp 0,9 => 0,899999999) 
*/