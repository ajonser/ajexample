<?php
require_once 'config.inc.php';
use AJExample\CurrencyManager;
use AJExample\CurrencyModel;

$currencyManager = new CurrencyManager();

$usd = $currencyManager->getLast(CurrencyModel::ENUM_USD);
$chf = $currencyManager->getLast(CurrencyModel::ENUM_CHF);

$allUsd = $currencyManager->getAll(CurrencyModel::ENUM_USD);
$allChf = $currencyManager->getAll(CurrencyModel::ENUM_CHF);

$dataSeriesUsd = array();
foreach ($allUsd as $model) {
    $dataSeriesUsd[] = [$model->getTimestamp(), $model->getRate()];
}

$dataSeriesChf = array();
foreach ($allChf as $model) {
    $dataSeriesChf[] = [$model->getTimestamp(), $model->getRate()];
}

?>
<html>
	<head>
		<meta http-equiv="Refresh" content="30">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<style>
            * {
	           font-family: 'Roboto', sans-serif;
            }
            body {
	           background-color: #edf2f6;
            }
            
            .ui-block, .highchart {
                background-color: #fff;
                border-radius: 5px;
                border: 1px solid #e6ecf5;
                margin-bottom: 15px;
            }
            
            .highchart {
	           height:300px;
            }
            
            .ui-block-title h3{
	           text-align:center;
            }
            
            .number {
	        	line-height:150px;
            	font-size:4em;
            	text-align:center;
            	display:block;
            }
        </style>
	</head>
	<body>
		<div class="container">
			<div class="row">
    			<div class="col-12">&nbsp;</div>
			</div>
    	</div>
    	<div class="container">
    		<div class="row">
    			<div class="col-6">
    				<div class="ui-block">
    					<div class="ui-block-title"><h3>EUR -> USD</h3></div>
    					<div class="ui-block-content">
    						<span class="number"><?php echo (!$usd) ? '---' : sprintf("%0.06f", $usd->getRate()); ?></span>
    					</div>
    				</div>
    			</div>
    			<div class="col-6">
    				<div class="ui-block">
    					<div class="ui-block-title"><h3>EUR -> CHF</h3></div>
    					<div class="ui-block-content">
    						<span class="number"><?php echo (!$chf) ? '---' : sprintf("%0.06f", $chf->getRate()); ?></span>
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-6">
    				<div class="highchart" data-title="Entwicklung USD" data-series='{"data": <?php echo json_encode($dataSeriesUsd);?>}' data-ytitle="USD"></div>
    			</div>
    			<div class="col-6">
    				<div class="highchart" data-title="Entwicklung CHF" data-series='{"data": <?php echo json_encode($dataSeriesChf);?>}' data-ytitle="CHF"></div>
    			</div>
    		</div>
		</div>
	</body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA=" crossorigin="anonymous"></script>
	<script src="https://code.highcharts.com/highcharts.src.js"></script>
	<script>
		$(document).ready(function() {

  		Highcharts.setOptions({
        	global:{useUTC:false},
        	lang: {
                decimalPoint: ',',
                thousandsSep: '.',
                loading: 'Daten werden geladen...',
                months: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                weekdays: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                shortMonths: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                exportButtonTitle: "Exportieren",
                printButtonTitle: "Drucken",
                rangeSelectorFrom: "Von",
                rangeSelectorTo: "Bis",
                rangeSelectorZoom: "Zeitraum",
                downloadPNG: 'Download als PNG-Bild',
                downloadJPEG: 'Download als JPEG-Bild',
                downloadPDF: 'Download als PDF-Dokument',
                downloadSVG: 'Download als SVG-Bild',
                resetZoom: "Zoom zurücksetzen",
                resetZoomTitle: "Zoom zurücksetzen"
            }
        });


			
			$('.highchart').each(function(){
				var dataSeries = $(this).data('series');

				var values = [];

				for (each in dataSeries.data) {
					var time = (new Date(dataSeries.data[each][0])).getTime();
					values.push([time * 1000, parseFloat(dataSeries.data[each][1])])
				}
				
				$(this).highcharts({
				    title: {
				        text: $(this).data('title')
				    },
				    yAxis: {
				        title: {
				            text: $(this).data('ytitle')
				        }
				    },
				    xAxis: {
				        type: 'datetime',
			        },
				    legend: {
				        layout: 'vertical',
				        align: 'right',
				        verticalAlign: 'middle'
				    },

				    series: [{
				        name: 'Entwicklung',
				        data: values
				    }]
				});
			});

		});
	</script>
</html>