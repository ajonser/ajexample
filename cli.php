<?php
/**
 * String
 *
 * PHP Version 7.1
 *
 * @category Commandline
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
require_once 'config.inc.php';

use AJExample\CurrencyModel as Model;
use AJExample\Db as DBCon;

/**
 * Versuche die aktuellsten Daten von https://currencylayer.com zu laden.
 * Danach erstelle für jede Währung ein CurrencyModel und speichere es in
 * der Datenbank. Durch den "insert ignore" werden doppelte Einträge nicht
 * als Fehler zurückgeliefert.
 * 
 * Exceptions werden abgefangen und die Nachricht ins Syslog weitergeleitet
 */
try {
    //Abruf und Aufbereitung der Währungen von https://currencylayer.com
    $fetched = \AJExample\CurrencyFetcher::getInstance()->fetchCurrentCurrencies();
    
    if (!is_null($fetched)) {
        $timestamp = $fetched['timestamp'];
        $database = DBCon::getInstance();
        
        foreach ($fetched['quotes'] as $currency => $rate) {
            $obj = new Model([
                'timestamp' => $timestamp,
                'currency' => $currency,
                'rate' => $rate
            ]);
             
            $database->insert($obj, true);
        }
        
    }
} catch (\Exception $e) {
    syslog(LOG_ERR, $e->getMessage());
}