<?php
/**
 * CurrencyModel
 *
 * PHP Version 7.1
 *
 * @category Model
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
namespace AJExample;

/**
 * CurrencyModel
 * 
 * @category Model
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 * 
 * @Table(name="rates")
 */
class CurrencyModel extends BaseModel
{

    /**#@+
     * Enums der Tabelle
     * 
     * @var integer
     */
    const ENUM_USD    = 1;
    const ENUM_CHF    = 2;
    /**#@-*/
    
    /**
     * Mapper von ENUM Wert zu ID
     * 
     * @var integer[]
     */
    const ENUM_VALUES = ['USD' => self::ENUM_USD, 'CHF' => self::ENUM_CHF];
    
    /**
     * @var string[]
     */
    const ENUM_NAMES  = [self::ENUM_USD => 'EURUSD', self::ENUM_CHF => 'EURCHF'];
    
    
    /**
     * Zeitstempel als UNIX-Timestamp
     * 
     * @var int
     */
    protected $timestamp;
    
    /**
     * Enum für die Währung
     * mögliche Werte: 0/1 
     * siehe Klassenconstanten
     * 
     * @var int
     */
    protected $currency;
    
    /**
     * @var string
     */
    protected $rate;
    
    /**
     * {@inheritDoc}
     * @see \AJExample\BaseModel::getDataRow()
     */
    public function getDataRow() : array
    {
        return [
            'timestamp' => $this->getTimestamp(),
            'currency'  => $this->getCurrency(),
            'rate'      => $this->getRate()
        ];
    }
    
    
    /**
     * 
     * @param int $timestamp Timestamp
     * 
     * @throws \InvalidArgumentException Wenn der Timestamp als negative Zahl übergeben wird
     * @throws \InvalidArgumentException Wenn der Timestamp älter als der 01.01.2019 00:00:00 ist
     * 
     * @return CurrencyModel
     */
    public function setTimestamp(int $timestamp) : CurrencyModel
    {
        if ($timestamp < 0) {
            throw new \InvalidArgumentException('Ungültiger Wert für Timestamp');
        }
        $checkup = new \DateTime('2019-01-01 00:00:00');
        if ($timestamp < $checkup->getTimestamp()) {
            throw new \InvalidArgumentException('Der Timestamp liegt zuweit in der Vergangenheit zurück');
        }
        
        $this->timestamp = $timestamp;
        return $this;
    }
    
    /**
     * Liefert den Timestamp als int
     * 
     * @return int
     */
    public function getTimestamp() : int
    {
        return (int)$this->timestamp;
    }
    
    /**
     * Setter für die Währung, kann als ID oder String ("USD", "CHF") übergeben werden
     * 
     * @param int|string $currency
     * 
     * @throws \InvalidArgumentException Wenn die Währung als ID übergeben wird und nicht zum Mapping passt
     * @throws \InvalidArgumentException Wenn die Währung als String übergeben wird und nicht zum Mapping passt
     * 
     * @return CurrencyModel
     */
    public function setCurrency($currency) : CurrencyModel
    {
        if (is_string($currency) && !is_numeric($currency)) {
            if (!isset(self::ENUM_VALUES[$currency])) {
                throw new \InvalidArgumentException('Ungültige Währung übergeben');
            }
            $currency = self::ENUM_VALUES[$currency];
        }
        
        if (!in_array($currency, [self::ENUM_USD, self::ENUM_CHF])) {
            throw new \InvalidArgumentException('Ungültige Währung übergeben');
        }
        
        $this->currency = $currency;
        return $this;
    }
    
    /**
     * Liefert die Währungs-ID
     * 
     * @return int
     */
    public function getCurrency() : int
    {
        return (int)$this->currency;
    }
    
    /**
     * Liefert die Währungsbezeichnung als String
     * oder Leerstring wenn nicht gesetzt
     * 
     * @return string
     */
    public function getCurrencyAsText() : string
    {
        if (!is_null($this->currency)) {
            return self::ENUM_NAMES[$this->currency];
        }
        return '';
    }
    
    /**
     * Setzt den Umrechnungskurs. Dies muss als String erfolgen
     * da als Float/Double es nicht gewährleistet werden kann, dass
     * der exakte Wert übergeben wird
     * 
     * @param string $rate
     * 
     * @throws \InvalidArgumentException Wenn der Wert leer oder keine Zahl als String ist
     * 
     * @return CurrencyModel
     */
    public function setRate(string $rate) : CurrencyModel
    {
        if (empty($rate) || !is_numeric($rate)) {
            throw new \InvalidArgumentException('Ungültiger Währungskurs übergeben');
        }
        
        $this->rate = $rate;
        return $this;
    }
    
    /**
     * Liefert den Umrechnungskurs
     * 
     * @return string
     */
    public function getRate() : string
    {
        return $this->rate;
    }
    
    
}