<?php
/**
 * BaseModel
 *
 * PHP Version 7.1
 *
 * @category Model
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
namespace AJExample;

use ReflectionClass;

/**
 * BaseModel
 * 
 * Basis Model von dem alle Models abgeleitet werden müssen.
 * Liefert den Basiskonstruktor zum Befüllen der Attribute.
 * 
 * @category Model
 * @author alexanderjonser
 */
abstract class BaseModel
{
    /**
     * Diese Methode muss jedes Model implementieren.
     * Liefert ein Array mit den Attributsnamen und deren Werte
     */
    abstract public function getDataRow() : array;
    
    /**
     * Basiskonstruktor.
     * 
     * Das übergebene besteht aus den Paaren "ATTRIBUTE" => "VALUE".
     * Die Schlüssel müssen den Attributsnamen im Model entsprechen und
     * für das Datenhandling müssen entsprechende Getter- und Setter-Methoden
     * implementiert werden
     * 
     * @param array|null $dataRow
     */
    public function __construct(array $dataRow = null)
    {
        if (!is_null($dataRow) && !empty($dataRow)) {
            foreach ($dataRow as $attribute => $value) {
                $setter = 'set' . ucfirst($attribute);
                if (method_exists($this, $setter)) {
                    $this->$setter($value);    
                }
            }
        }
    }
    
    /**
     * Liefert den Namen der Tabelle, in der die Daten des Models gespeichert werden.
     * Die Konfiguration wird im Doc-Block der Klasse erwartet und zwar im Format
     * 
     * @Table("name"="TABELLENNAME")
     * 
     * @throws \ErrorException
     * @return boolean|string
     */
    public function getTableName() 
    {
        $tablename = false;
        
        $reflect = new \ReflectionClass($this);
        $doc     = $reflect->getDocComment();
        if (preg_match('/\@Table\(name\=(.*)\)/s', $doc, $matches)) {
            $tablename = strtr($matches[1], ['"' => '']); 
        }
        
        if (empty($tablename) || $tablename === false) {
            throw new \ErrorException('Tabellenname nicht dokumentiert');
        }
        
        return $tablename;
    }
    
    /**
     * String-Darstellung eines Models
     * 
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getDataRow());
    }
    
}

