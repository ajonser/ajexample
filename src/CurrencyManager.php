<?php
/**
 * CurrencyManager
 *
 * PHP Version 7.1
 *
 * @category Manager
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
namespace AJExample;

use AJExample\Db;
use AJExample\CurrencyModel;

/**
 * CurrencyManager
 * 
 * Manager Klasse zur Verarbeitung des CurrencyModels
 * 
 * @category Manager
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
class CurrencyManager
{

    /**
     * Datenbankconnect
     * 
     * @var \AJExample\Db
     */
    protected $dbCon = null;
    
    /**
     * Liefert die DB-Verbindung bzw. stellt diese her wenn noch nicht vorhanden
     * 
     * @return \AJExample\Db
     */
    private function _getDbConnection()
    {
        if (is_null($this->dbCon)) {
            $this->dbCon = DB::getInstance();
        }
        return $this->dbCon;
    }
    
    /**
     * Liefert den letzten Eintrag einer bestimmten Währung aus der Tabelle
     * oder FALSE wenn noch keiner vorhanden.
     * 
     * @param int $currency Enum aus \AJExample\CurrencyModel
     * 
     * @return \AJExample\CurrencyModel|boolean
     */
    public function getLast(int $currency) {
        $dbCon = $this->_getDbConnection();
        $data = $dbCon->executeQuery(
            "SELECT * FROM rates WHERE timestamp = (SELECT max(timestamp) FROM rates) AND currency = :currency;",
            ['currency' => $currency]
        );
        
        if ($data && !empty($data)) {
            $data = array_pop($data);
            return new CurrencyModel($data);
        }
        return false;
    }
    
    /**
     * Liefert alle Einträge einer bestimmten Währung aus der Tabelle,
     * sortiert anhand des gespeicherten Timestamps.
     * 
     * @param int $currency Enum aus \AJExample\CurrencyModel
     * 
     * @return \AJExample\CurrencyModel[]
     */
    public function getAll(int $currency) {
        $dbCon = $this->_getDbConnection();
        $data = $dbCon->executeQuery(
            "SELECT * FROM rates WHERE currency = :currency ORDER BY timestamp DESC;",
            ['currency' => $currency]
        );
        
        $models = array();
        if ($data && !empty($data)) {
            foreach ($data as $row) {
                $models[] = new CurrencyModel($row);
            }
        }
        
        return $models;
    }
    
}