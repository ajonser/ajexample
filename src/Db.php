<?php
/**
 * Db
 *
 * PHP Version 7.1
 *
 * @category Helper
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
namespace AJExample;

use AJExample\BaseModel;

/**
 * Db
 *
 * Singleton-Klasse für den Datenbank-Connect
 *
 * @category Helper
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
class Db {
    
    /**
     * Instance-Keeper
     *
     * @var \AJExample\Db
     */
    private static $instance = null;
    
    /**
     * PDO-Instance
     *
     * @var \PDO
     */
    private $connection = null;
    
    
    /**
     * Konstruktor
     *
     * Versucht anhand der konfigurierten Zugangsdaten einen Connect zur Datenbank aufzubauen
     */
    private function __construct()
    {
        try {
            $dsn = sprintf("mysql:host=%s;dbname=%s;charset=utf8", DB_HOST, DB_NAME);
            $this->connection = new \PDO(
                $dsn, DB_USER, DB_PWD, 
                [
                    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                    \PDO::ATTR_EMULATE_PREPARES   => false,
                ]
            );
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        
    }
    
    /**
     * Liefert die Instanz des Singleton
     *
     * @return \AJExample\Db
     */
    public static function getInstance() : Db
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Insert Methode
     * 
     * Nutzt die Daten aus getDataRow des Models um einen neuen Eintrag in der Tabelle 
     * zu speichern. Hat das Model ein Attribut Id, wird davon ausgegangen, dass dies
     * mit autoincrement in der Tabelle konfiguriert wurde und nach erfolgreichem Speichern
     * wird die "last inserted id" dem Model übergeben.
     * 
     * Mit ignoreAble == true wird sichergestellt, dass bei auftreten von doppelten Werten
     * kein Fehler geworfen wird.
     * 
     * @param BaseModel $object
     * @param boolean   $ignoreAble 
     * 
     * @return \AJExample\BaseModel|boolean
     */
    public function insert(BaseModel $object, $ignoreAble = false)
    {
        //Lade die Daten
        $data = $object->getDataRow();
        
        //Tabellenspalten definieren
        $fields = array_keys($data);
        
        //Parameter für das Prepared-Statement aufbereiten
        $params = array_keys($data);
        array_walk($params, function(&$a){ $a = ':' . $a; });
        
        //Basis-Query aufbauen
        $baseQuery = "INSERT INTO %s (%s) VALUES (%s);";
        if ($ignoreAble === true) {
            $baseQuery = "INSERT IGNORE INTO %s (%s) VALUES (%s);";
        }
            
        //Insert-Query aufbauen
        $query = sprintf($baseQuery, $object->getTableName(), implode(',', $fields), implode(',', $params));
        
        //Query ausführen
        $stmt = $this->_execute($query, $data);
        
        //Wenn autoincrement vorhanden dann id setzten
        if ($stmt && $stmt->rowCount() > 0) {
            if (method_exists($object, 'setId')) {
                $object->setId($this->connection->lastInsertId());
            }
            return $object;
        }
        
        return false;
    }
    
    /**
     * Query ausführen
     * 
     * @param string $query  Statement
     * @param array  $params Parameter
     * 
     * @return array
     */
    public function executeQuery(string $query, array $params = null) : array
    {
        $data = array();
        $stmt = $this->_execute($query, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    

    
    /**
     * Erstellt das Prepared-Statement und bindet die Parameter.
     * Hierbei wird sichergestellt, dass Integer auch als Integer und nicht
     * als String übergeben werden
     * 
     * @param string $query
     * @param array  $params
     * 
     * @return \PDOStatement
     */
    private function _execute(string $query, array $params = null) : \PDOStatement
    {
        $stmt = $this->connection->prepare($query);
        foreach ($params as $param => $value) {
            $paramType = \PDO::PARAM_STR;
            if (is_int($value)) {
                $paramType = \PDO::PARAM_INT;
            }
            $stmt->bindValue(':'.$param, $value, $paramType);
        }
        $stmt->execute();
        return $stmt;
    }
    
}