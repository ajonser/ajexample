<?php
/**
 * CurrencyFetcher
 *
 * PHP Version 7.1
 *
 * @category Helper
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
namespace AJExample;

use OceanApplications\currencylayer\client as CurrencyClient;
use AJExample\CurrencyModel;

/**
 * CurrencyFetcher
 *
 * Hilfsklasse zum Abruf der API von https://currencylayer.com
 *
 * @category Helper
 * @author   Alexander Jonser <a.jonser@googlemail.com>
 */
class CurrencyFetcher {

    /**
     * Instance-Keeper
     *
     * @var \AJExample\CurrencyFetcher
     */
    private static $instance = null;
    
    /**
     * Client-Instanz aus OceanApplications\currencylayer\client
     *
     * @var currencylayer\client
     */
    private $client = null;
    
 
    /**
     * Konstruktor
     * 
     * Prüft zunächst ob der API-Key konfiguriert wurde. 
     * Danach wird eine neue Instanz des API-Clients initiert
     * 
     * @throws \ErrorException API_KEY nicht definiert oder leer
     */
    private function __construct()
    {
        if (!defined('API_KEY') && !empty(API_KEY)) {
            throw new \ErrorException('API Key nicht konfiguriert');
        }
        $this->client = new CurrencyClient(API_KEY);
        
    }
    
    /**
     * Liefert die Instanz des Singleton
     *
     * @return \AJExample\CurrencyFetcher
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Lädt von der LIVE-Api die aktuellen Daten von USD => EUR und USD => CHF
     * Da die Free-Version der API es nicht erlaubt den Source von USD auf eine
     * andere Währung zu ändern, muss dann manuell umgerechnet werden.
     * 
     * Inhalt einer erfolgreichen Abfrage:
     * {
     *   "success":true,
     *    "terms":"https:\/\/currencylayer.com\/terms",
     *    "privacy":"https:\/\/currencylayer.com\/privacy",
     *    "timestamp":1550685546,
     *    "source":"USD",
     *    "quotes":{
     *        "USDEUR":0.879775,
     *        "USDCHF":0.998445
     *    }
     * }
     * 
     * Im Rückgabewerte wird quotes durch die Methode _convert um
     * EURUSD und EURCHF erweitert
     * <code>
     * array(
     *  'timestamp' => integer,
     *  'quotes' => [
     *      ENUM_USD => string,
     *      ENUM_CHF => string
     *  ] 
     * )
     * </code>
     * 
     * @return array
     */
    public function fetchCurrentCurrencies(): ?array
    {
        //Lade zunächst die aktuellsten Werte, Rückgabewert ist ein Array
        $aCurrencyRates = $this->client
            ->currencies('EUR,CHF')
            ->live();
           
        //Füge dem Array noch die Umrechnungen EUR->USD und EUR->CHF hinzu
        try {
            $this->_convert($aCurrencyRates);
        } catch (\ErrorException $e) {
            syslog(LOG_ERR, $e->getMessage());
            return null;
        }
        
        return [
            'timestamp' => $aCurrencyRates['timestamp'],
            'quotes'    => [
                CurrencyModel::ENUM_USD => $aCurrencyRates['quotes']['EURUSD'],
                CurrencyModel::ENUM_CHF => $aCurrencyRates['quotes']['EURCHF'],
            ]
        ];
    }
    
    /**
     * Diese Funktion wird genutzt um von USD->EUR und USD->CHF
     * auf EUR->USD und EUR->CHF umzurechnen.
     * 
     * Hierfür ist die Extension bcmath erforderlich
     * 
     * @param array &$aCurrencyRates Der in Array umgewandelte JSON-String von der API
     * 
     * @return void
     */
    private function _convert(array &$aCurrencyRates) : void
    {
        //Man könnte auch manuell den source auf USD stellen, da im Free-Account als source nur USD erlaubt ist
        $source = $aCurrencyRates['source'];
        $quotes = $aCurrencyRates['quotes'];
        
        $usd2EUR = (isset($quotes[$source.'EUR'])) ? $quotes[$source.'EUR'] : 0.0;
        $usd2CHF = (isset($quotes[$source.'CHF'])) ? $quotes[$source.'CHF'] : 0.0;
        
        if ($usd2EUR <= 0.0) {
            throw new \Exception('Keine Umrechnung von USD -> EUR vorhanden');
        }
        
        $eur2USD = \bcdiv('1', "$usd2EUR", 6);
        $eur2CHF = \bcmul("$eur2USD", "$usd2CHF", 6);
        
        $aCurrencyRates['quotes']['EURUSD'] = $eur2USD;
        $aCurrencyRates['quotes']['EURCHF'] = $eur2CHF;
        
    }
    
}